<?php _e('Your listing has been successfully updated. Listing information is shown below.', 'AWPCP') ?> 

<?php if (!empty($message)): ?>
<?php echo $message ?> 
<?php endif ?>

<?php _e("Title", "AWPCP") ?>: <?php echo $ad->get_title() ?> 
<?php _e("Post URL", "AWPCP") ?>: <?php echo urldecode( url_showad( $ad->ad_id ) ); ?> 
<?php _e("Email", "AWPCP") ?>: <?php echo $ad->ad_contact_email ?> 
<?php if ( get_awpcp_option( 'include-ad-access-key' ) ): ?>
<?php _e( "Access Key", "AWPCP" ); ?>: <?php echo $ad->get_access_key(); ?> 
<?php endif; ?>
<?php _e("Expiry Date", "AWPCP") ?>: <?php echo $ad->get_end_date() ?>


<?php echo sprintf(__("You can use the access key above to edit or delete your post on the website. If you have questions about your listing contact us at %s.", 'AWPCP'), $admin_email) ?> 

<?php echo awpcp_get_blog_name() ?> 
<?php echo home_url() ?> 
