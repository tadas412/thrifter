<?php echo get_awpcp_option('listingaddedbody') ?> 

<?php _e("Title", "AWPCP") ?>: <?php echo $ad->ad_title ?> 
<?php _e("Post URL", "AWPCP") ?>: <?php echo urldecode( url_showad( $ad->ad_id ) ); ?>  
<?php _e("Email", "AWPCP") ?>: <?php echo $ad->ad_contact_email ?> 
<?php if ( $include_listing_access_key ): ?>
<?php _e( "Access Key", "AWPCP" ); ?>: <?php echo $ad->get_access_key(); ?> 
<?php endif; ?>
<?php _e("Expiry Date", "AWPCP") ?>: <?php echo $ad->get_end_date() ?>

<?php if ($transaction): ?>
<?php   if ($transaction->get('txn-id')): ?>
<?php _e("Payment Transaction", "AWPCP")?>: <?php echo $transaction->get('txn-id') ?> 
<?php   endif ?>
<?php   if ( $show_total_amount ): ?>
<?php echo esc_html( __( 'Order Total', 'AWPCP' ) ); ?> (<?php echo esc_html( $currency_code ); ?>): <?php echo esc_html( awpcp_format_money( $total_amount ) ); ?> 
<?php   endif; ?>
<?php   if ( $show_total_credits ): ?>
<?php echo esc_html( __( 'Order Total (credits)', 'AWPCP' ) ); ?>: <?php echo esc_html( $total_credits ); ?> 
<?php   endif; ?>

<?php endif ?>
<?php if ( $include_edit_listing_url ): ?>
<?php _e( 'The next link will take you to a page where you can edit the listing:', 'AWPCP' ); ?> 

<?php echo awpcp_get_edit_listing_url( $ad ); ?> 

<?php endif; ?>
<?php if (!empty($message)): ?>
<?php _e('Additional Details', 'AWPCP') ?> 

<?php echo $message ?> 

<?php endif ?>
<?php echo sprintf(__("You can use the access key above to edit or delete your post on the website. If you have questions about your listing contact us at %s.", 'AWPCP'), $admin_email) ?> 

<?php echo $blog_name; ?> 
<?php echo home_url(); ?> 
