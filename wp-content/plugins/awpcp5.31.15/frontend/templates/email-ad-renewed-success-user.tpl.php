<?php // emails are sent in plain text, blank lines in templates are required ?>
<?php echo $introduction ?>


<?php _e("Title", "AWPCP") ?>: <?php echo $ad->ad_title ?>

<?php _e("Post URL", "AWPCP") ?>: <?php echo urldecode( url_showad( $ad->ad_id ) ); ?>

<?php _e("Email", "AWPCP") ?>: <?php echo $ad->ad_contact_email ?>

<?php if ( get_awpcp_option( 'include-ad-access-key' ) ): ?>
<?php _e("Access Key", "AWPCP") ?>: <?php echo $ad->get_access_key() ?>
<?php endif; ?>

<?php _e("Expiry Date", "AWPCP") ?>: <?php echo $ad->get_end_date() ?>



<?php
    $text = __("You can use the access key above to edit or delete your post on the website. If you have questions about your listing contact us at %s.", 'AWPCP');
    echo sprintf( $text, awpcp_admin_recipient_email_address() );
?>




<?php echo home_url() ?>
